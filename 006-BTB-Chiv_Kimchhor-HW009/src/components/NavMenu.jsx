import React from 'react'
import {Navbar,NavDropdown,Nav,Button,Form,FormControl, Container} from 'react-bootstrap'
import { NavLink } from 'react-router-dom'

export default function NavMenu() {
    return (
        <Navbar bg="light" expand="lg">
        <Container>
        <Navbar.Brand href="#home">React-Router</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
            <Nav.Link as = {NavLink} to ="/">Home</Nav.Link>
            <Nav.Link as = {NavLink} to ="/video">Video</Nav.Link>
            <Nav.Link as = {NavLink} to ="/account">Account</Nav.Link>
            <Nav.Link as = {NavLink} to ="/welcome">Welcome</Nav.Link>
            <Nav.Link as = {NavLink} to ="/auth">Auth</Nav.Link>
            </Nav>
            <Form inline>
            <FormControl type="text" placeholder="Search" />
            </Form>
        </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}
