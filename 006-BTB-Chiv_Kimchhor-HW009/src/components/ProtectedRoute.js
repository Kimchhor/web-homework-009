import { Redirect, Route } from "react-router"
import authentication from "../auth/authentication"


function ProtectedRoute({children,...props}){
    return (
     <Route {...props} render={()=>authentication.isAuth? children :<Redirect to='/auth'/>}/>
        // <Route {...props} render={()=> children}/>

    )
}
export default ProtectedRoute