import React from 'react'
import {Card,Button, Row,Col} from 'react-bootstrap'
import { Link } from 'react-router-dom'
function Item({data}) {
    return(
        <>
        {data.map((value,id)=>
        <div>
           
              <Col lg={3} key={id} >
                <Card >
                    <Card.Img variant="top" src={value.img} style={{height:'220px'}} />
                    <Card.Body>
                        <Card.Title>{value.title}</Card.Title>
                        <Card.Text>
                        {value.description}
                        </Card.Text>
                        <Link to={`/detail/${id+1}`}>
                            <Button variant="primary">Read</Button>
                        </Link>
                        
                    </Card.Body>
                </Card>
                </Col> 
                </div>
         
               
                
                )}
                </>
    )
    
    
}

export default Item
