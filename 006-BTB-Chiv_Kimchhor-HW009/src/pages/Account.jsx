import React from 'react'
import { Route, Switch, useParams } from 'react-router'
import { Link } from 'react-router-dom'
// import ChildAccount from './ChildAccount'
export default function Account() {
    return (
        <div>
            <h3>Account</h3>
            <ul>

                <li><Link to="/account/netflix">Netflix</Link></li>
                <li><Link to="/account/zillow_group">Zillow Group</Link></li>
                <li><Link to="/account/yahoo">Yahoo</Link></li>
                <li><Link to="/account/modus_create">Modus Create</Link></li>
            </ul>

            <Switch>
            <Route path="/account/:id" children={<Child />} />
            </Switch>
        </div>
    )
}

function Child() {
    // We can use the `useParams` hook here to access
    // the dynamic pieces of the URL.
    let { id } = useParams()
  
    return (
      <div>
        <h3>ID: {id}</h3>
      </div>
    );
  }
