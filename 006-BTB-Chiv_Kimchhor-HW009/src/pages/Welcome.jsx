import React from "react";
import { Button} from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import authentication from "../auth/authentication";


export default function Welcome() {
  let history=useHistory();
  return (
    <div>
        <h3>Welcome</h3>
        <Button onClick={()=>{
          authentication.logout(()=> {
            history.push('/auth')
          })
        }}>Logout</Button>
       
    </div>
  );
}
