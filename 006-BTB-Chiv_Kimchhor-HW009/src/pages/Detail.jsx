import React from 'react'
import { useParams } from 'react-router'

export default function Detail() {
    let {id}=useParams();
    return (
        <div>
            <h3>Detail {id}</h3>
        </div>
    )
}
