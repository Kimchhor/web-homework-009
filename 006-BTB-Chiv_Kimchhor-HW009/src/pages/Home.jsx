import React from 'react'
import Item from '../components/Item'
import { useState } from 'react';
export default function Home() {

    const [data,setData]=useState([
        {
          
          img : "https://upload.wikimedia.org/wikipedia/en/d/df/Nakitai_Watashi_wa_Neko_o_Kaburu_poster.png",
          title : "A Whisker Away",
          description : "A Whisker Away, known in Japan as Nakitai Watashi wa Neko wo Kaburu (泣きたい私は猫をかぶる, lit. (Wanting to Cry, I Pretend to Be a Cat), is a 2020 Japanese animated film produced by Studio Colorido."
        },
        {
          
          img : "https://ychef.files.bbci.co.uk/976x549/p09f3lcg.jpg",
          title : "Spirited Away",
          description : "Spirited Away (Japanese: 千と千尋の神隠し, Hepburn: Sen to Chihiro no Kamikakushi, 'Sen and Chihiro’s Spiriting Away') is a 2001 Japanese animated fantasy film written and directed by Hayao Miyazaki. "
        },
        {
         
          img : "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQbYpI4mYx8iImEc1TVNqY5iJFREmo7-sGK7PdtBztb2qtS0Pzb",
          title : "Demon Slayer",
          description : "Demon Slayer: Kimetsu no Yaiba (鬼滅の刃, Kimetsu no Yaiba, lit. Blade of Demon Destruction[4]) is a Japanese manga series written and illustrated by Koyoharu Gotouge."
        },
        {
        
          img : "https://upload.wikimedia.org/wikipedia/en/0/0b/Your_Name_poster.png",
          title : "Your Name",
          description : "Your Name (Japanese: 君の名は。, Hepburn: Kimi no Na wa) is a 2016 Japanese animated romantic fantasy film produced by CoMix Wave Films and released by Toho. "
    
          
        },
        {
          
          img : "https://images-na.ssl-images-amazon.com/images/I/91DoU3%2BT-6L._SL1500_.jpg",
          title : "My Neighbor Totoro",
          description : "My Neighbor Totoro (Japanese: となりのトトロ, Hepburn: Tonari no Totoro) is a 1988 Japanese animated fantasy film written and directed by Hayao Miyazaki and animated by Studio Ghibli for Tokuma Shoten. "
        },
        {
          
          img : "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSxo7Naxu0tjuSEZ9_faYL--aWjx8V5TKr4q2YeenYKXXik-T5P",
          title : "Alita Battle Angel",
          description : "Alita: Battle Angel is a 2019 American cyberpunk action film based on Japanese manga artist Yukito Kishiro's 1990s series Battle Angel Alita and its 1993 original video animation adaptation, Battle Angel." 
        }
      ])
    return (
        <div>
           <Item data={data} />
        </div>
    )
}
