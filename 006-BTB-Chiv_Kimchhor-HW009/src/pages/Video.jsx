import React from "react";
import { Button, ButtonGroup } from "react-bootstrap";
import { Link, Route, Switch, useParams } from "react-router-dom";
import useQuery from "../custom/useQuery";

export default function Video() {
  return (
    <div>
      <h3>Video</h3>
      <ButtonGroup aria-label="Basic example">
        <Link to="/video/movie">
          <Button variant="secondary">Movie</Button>
        </Link>
        <Link to="/video/animation">
          <Button variant="secondary">Animation</Button>
        </Link>
      </ButtonGroup>
      <Switch>
        <Route path="/video/movie" children={<Movie />} />
        <Route path="/video/animation" children={<Animation />} />
      </Switch>
    </div>
  );
}
function Movie() {
  let query = useQuery();
  // console.log("type : ", query.get("type"));
  return (
    <div>
      <h3>Movie Category</h3>
      <ButtonGroup aria-label="Basic example">
        <Link to="/video/movie?type=Advanture">
          <Button variant="secondary">Advanture</Button>
        </Link>
        <Link to="/video/movie?type=Crime">
          <Button variant="secondary">Crime</Button>
        </Link>

        <Link to="/video/movie?type=Action">
          <Button variant="secondary">Action</Button>
        </Link>

        <Link to="/video/movie?type=Romance">
          <Button variant="secondary">Romance</Button>
        </Link>

        <Link to="/video/movie?type=Comedy">
          <Button variant="secondary">Comedy</Button>
        </Link>
      </ButtonGroup>

      <h5>
        Please Choose Category :{" "}
        <div style={{ color: "red", display: "inline" }}>
          {query.get("type")}
        </div>{" "}
      </h5>
    </div>
  );
}

function Animation() {
  let query = useQuery();
  return (
    <div>
      <h3>Animation Category</h3>
      <ButtonGroup aria-label="Basic example">
        <Link to="/video/animation?type=Action">
          <Button variant="secondary">Action</Button>
        </Link>

        <Link to="/video/animation?type=Romance">
          <Button variant="secondary">Romance</Button>
        </Link>

        <Link to="/video/animation?type=Comedy">
          <Button variant="secondary">Comedy</Button>
        </Link>
      </ButtonGroup>
      <h5>
        Please Choose Category:{" "}
        <div style={{ color: "red", display: "inline" }}>
          {query.get("type")}
        </div>{" "}
      </h5>
    </div>
  );
}
