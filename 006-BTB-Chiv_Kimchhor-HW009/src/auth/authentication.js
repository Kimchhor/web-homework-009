class Authentication{
    constructor(){
        this.isAuth=false
    }
    login(cb){
        this.isAuth=true
        cb()
    }
    logout(cb){
        this.isAuth=false
        cb()
    }
}

export default new Authentication()