import { Col, Container, Row } from "react-bootstrap";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import NavMenu from "./components/NavMenu";
import Account from "./pages/Account";
import Video from "./pages/Video";
import Welcome from "./pages/Welcome";
import Auth from "./pages/Auth";
import Home from "./pages/Home";
import Detail from "./pages/Detail";
import ProtectedRoute from "./components/ProtectedRoute";
function App(data) {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Container>
            <NavMenu />

            <Route exact path="/" component={Home} />
            <Route path="/video" component={Video} />
            <Route path="/account" component={Account} />
            <ProtectedRoute path="/welcome">
              {" "}
              <Welcome />{" "}
            </ProtectedRoute>
            <Route path="/auth" component={Auth} />
            <Route path="/detail/:id" component={Detail} />
          </Container>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
